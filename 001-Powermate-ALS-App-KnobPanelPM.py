from __future__ import print_function 
    #***
  #*********************************************************************
#*************************************************************************
#*** 
#*** GizmoDaemon Config Script
#***    Powermate ALS.App.KnobPanelPM config
#***
#*****************************************
  #*****************************************
    #***

"""

  Copyright (c) 2007, Gizmo Daemon Team
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at 

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and 
  limitations under the License. 
  
"""

############################
# Imports
##########################

from GizmoDaemon import *
from GizmoScriptActiveApplication import *
import datetime # debug: print date; elam3; print(datetime.datetime.now())
import pprint
pp = pprint.PrettyPrinter(indent=4)

ENABLED = True
VERSION_NEEDED = 3.2
INTERESTED_CLASSES = [GizmoEventClass.Powermate]
INTERESTED_WINDOWS = ["firefox", "gedit", "kwrite", "ALS.App.KnobPanelPM"]

############################
# PowermateALS Class definition
##########################

class PowermateALS(GizmoScriptActiveApplication):
    """
    ALS.App.KnobPanelPM Powermate Event Mapping
    """

    ############################
    # Public Functions
    ##########################

    def onDeviceEvent(self, Event, Gizmo = None):
        """
        Called from Base Class' onEvent method.
        See GizmodDispatcher.onEvent documention for an explanation of this function
        """

        if Gizmo.DeviceClassID == 0:
            ###  Device 1 ###
            # Clockwise:                F4
            # Counter-clockwise:        F3
            # ClickClockwise:           W
            # ClickCounter-clockwise:   Q
            #
            # Check for rotations
            if Event.Type == GizmoEventType.EV_REL:
                if not Gizmo.getKeyState(GizmoKey.BTN_0):
                    # Button Is Not Pressed.
                    if Event.Value > 0:
                        #for repeat in range(abs(Event.Value)):
                        Gizmod.Keyboards[0].createEvent(GizmoEventType.EV_KEY, GizmoKey.KEY_4)
                    else:
                        #for repeat in range(abs(Event.Value)):
                        Gizmod.Keyboards[0].createEvent(GizmoEventType.EV_KEY, GizmoKey.KEY_3)
                else:
                    # Button Is Pressed.
                    if Event.Value > 0:
                        #for repeat in range(abs(Event.Value)):
                        Gizmod.Keyboards[0].createEvent(GizmoEventType.EV_KEY, GizmoKey.KEY_W)
                    else:
                        #for repeat in range(abs(Event.Value)):
                        Gizmod.Keyboards[0].createEvent(GizmoEventType.EV_KEY, GizmoKey.KEY_Q)
                return True
        elif Gizmo.DeviceClassID == 1:
            ###  Device 2 ###
            # Clockwise:                F8
            # Counter-clockwise:        F7
            # ClickClockwise:           U
            # ClickCounter-clockwise:   Y
            if Event.Type == GizmoEventType.EV_REL:
                if not Gizmo.getKeyState(GizmoKey.BTN_0):
                    # Button Is Not Pressed.
                    if Event.Value > 0:
                        #for repeat in range(abs(Event.Value)):
                        Gizmod.Keyboards[0].createEvent(GizmoEventType.EV_KEY, GizmoKey.KEY_8)
                    else:
                        #for repeat in range(abs(Event.Value)):
                        Gizmod.Keyboards[0].createEvent(GizmoEventType.EV_KEY, GizmoKey.KEY_7)
                else:
                    # Button Is Pressed.
                    if Event.Value > 0:
                        #for repeat in range(abs(Event.Value)):
                        Gizmod.Keyboards[0].createEvent(GizmoEventType.EV_KEY, GizmoKey.KEY_U)
                    else:
                        #for repeat in range(abs(Event.Value)):
                        Gizmod.Keyboards[0].createEvent(GizmoEventType.EV_KEY, GizmoKey.KEY_Y)
                return True
        else:
            return False

    
    ############################
    # Private Functions
    ##########################



    def __init__(self):
        """ 
        Default Constructor
        """
        
        GizmoScriptActiveApplication.__init__(self, ENABLED, VERSION_NEEDED, INTERESTED_CLASSES, INTERESTED_WINDOWS)



############################
# PowermateALS class end
##########################

# register the user script
PowermateALS()

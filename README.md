# Griffin Powermate for ALS Apps on Centos 7

## (In-Progress)
* 000-Powermate-ALS-App-KnobPanelPM.py

### Device 1
* ClockwiseRotate: F4
* CounterClockwiseRotate: F3
* Click + ClockwiseRotate: W
* Click + CounterClockwiseRotate: Q

### Device 2
* ClockwiseRotate: F8
* CounterClockwiseRotate: F7
* Click + ClockwiseRotate: U
* Click + CounterClockwiseRotate: Y
